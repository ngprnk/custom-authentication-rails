class UsersController < ApplicationController
	before_action :require_user, only: [:afterlog]

	def new
		@user=User.new
	end

	def create
		@user=User.new(user_params)
		if @user.save
			session[:user_id]=@user.id 
			redirect_to afterlog_path
		else 
			redirect_to signup_path
		end

	end

	def afterlog
	end

	def home
	end


	private
	def user_params
		params.require(:user).permit(:first_name,:last_name,:email,:password)
	end


end
